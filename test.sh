set -xv

##set -e
#
#curl -i http://localhost:8082/restaurantes/mais-proximos/71503510
#curl -i http://localhost:8082/restaurantes/mais-proximos/71503510/tipos-de-cozinha/1
#curl -i http://localhost:8082/restaurantes/71503510/restaurante/1
#
#curl -X POST -i -H 'Content-Type: application/json' \
#-d '{"valor": 51.8, "nome": "JOÃO DA SILVA", "numero": "1111 2222 3333 4444", "expiracao": "2022-07", "codigo": "123",
#"formaDePagamentoId": 2, "pedidoId": 1}' \
#http://localhost:8081/pagamentos
#
#echo "http://localhost:9999/pagamentos/1"
#curl -i http://localhost:9999/pagamentos/1
#
#echo "http://localhost:9999/distancia/restaurantes/mais-proximos/71503510"
#curl -i http://localhost:9999/distancia/restaurantes/mais-proximos/71503510
#
#echo "http://localhost:9999/restaurantes/1"
#curl -i http://localhost:9999/restaurantes/1
#
#echo "http://localhost:9999/restaurantes-com-distancia/71503510/restaurante/1"
#curl -i http://localhost:9999/restaurantes-com-distancia/71503510/restaurante/1

#curl -X POST -i -H 'Content-Type: application/json' -d '{"valor": 51.8, "nome": "JOÃO DA SILVA", "numero": "1111 2222 3333 4444", "expiracao": "2022-07", "codigo": "123", "formaDePagamentoId": 2, "pedidoId": 1}' http://localhost:9999/pagamentos

curl -iv http://localhost:9999/pagamentos/1
curl -iv http://localhost:9999/distancia/restaurantes/mais-proximos/71503510
curl -iv http://localhost:9999/restaurantes/1
curl -v -X PUT -i http://localhost:8081/pagamentos/1
curl -v -X GET http://localhost:9090/restaurantes/1
curl -v -X GET http://localhost:8080/restaurantes/1
curl -v -X PUT -i http://localhost:8080/pedidos/1/pago
curl -v -X PUT -i http://localhost:9090/pedidos/1/pago
curl -v -X GET http://localhost:8080/pedidos/1
curl -v -X GET http://localhost:9090/pedidos/1
