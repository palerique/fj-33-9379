pushd fj33-eats-monolito-modular || exit
  mvn clean install -U
popd || exit

pushd fj33-eats-pagamento-service || exit
  mvn clean install -U
popd || exit

pushd fj33-eats-distancia-service || exit
  mvn clean install -U
popd || exit

pushd fj33-eats-ui || exit
#  nvm use 10
  npm install
  npm run build
popd || exit
